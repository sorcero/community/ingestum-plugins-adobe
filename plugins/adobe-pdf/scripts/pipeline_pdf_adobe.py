#!/usr/bin/python3
#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#

import json
import argparse
import tempfile

from ingestum import engine
from ingestum import manifests
from ingestum import pipelines
from ingestum import transformers


def generate_pipeline():
    pipeline = pipelines.base.Pipeline(
        name="pipeline_pdf_adobe",
        pipes=[
            # Extract all of the text, tables, and images from the PDF,
            # formatting the tables into markdown and saving the images into
            # the output directory using the Adobe PDF Extract API.
            pipelines.base.Pipe(
                name="text",
                sources=[
                    pipelines.sources.Manifest(source="pdf"),
                ],
                steps=[
                    # We can use the layout markers to identify specific parts
                    # of the PDF like headers and figures. In this case, we
                    # just want to split each section into passages so we use
                    # the same marker for all element types.
                    transformers.PDFSourceCreateTextDocumentAdobe(
                        directory="output",
                        first_page=-1,
                        last_page=-1,
                        layout_markers={
                            "title": "PASSAGE_MARKER",
                            "header": "PASSAGE_MARKER",
                            "table": "PASSAGE_MARKER",
                            "figure": "PASSAGE_MARKER",
                            "list_item": "PASSAGE_MARKER",
                            "toc_item": "PASSAGE_MARKER",
                            "footnote": "PASSAGE_MARKER",
                            "paragraph": "PASSAGE_MARKER",
                        },
                    ),
                    # Remove empty passages.
                    transformers.TextDocumentStringReplace(
                        regexp="PASSAGE_MARKER\s*PASSAGE_MARKER",
                        replacement="PASSAGE_MARKER",
                    ),
                    transformers.TextDocumentStringReplace(
                        regexp="\APASSAGE_MARKER", replacement=""
                    ),
                    transformers.TextDocumentStringReplace(
                        regexp="PASSAGE_MARKER\Z", replacement=""
                    ),
                    # Split the document into passages.
                    transformers.TextSplitIntoCollectionDocument(
                        separator="PASSAGE_MARKER"
                    ),
                    transformers.CollectionDocumentTransform(
                        transformer=transformers.TextCreatePassageDocument()
                    ),
                    # Here we add a tag to indicate that the text came from the
                    # Adobe API.
                    transformers.CollectionDocumentTransform(
                        transformer=transformers.PassageDocumentAddMetadata(
                            key="tags", value="adobe"
                        )
                    ),
                ],
            ),
        ],
    )
    return pipeline


def ingest(path, first_page, last_page):
    destination = tempfile.TemporaryDirectory()

    manifest = manifests.base.Manifest(
        sources=[
            manifests.sources.PDF(
                id="id",
                pipeline="pipeline_pdf_adobe",
                first_page=first_page,
                last_page=last_page,
                location=manifests.sources.locations.Local(
                    path=path,
                ),
                destination=manifests.sources.destinations.Local(
                    directory=destination.name,
                ),
            )
        ]
    )

    pipeline = generate_pipeline()

    results, _ = engine.run(
        manifest=manifest,
        pipelines=[pipeline],
        pipelines_dir=None,
        artifacts_dir=None,
        workspace_dir=None,
    )

    destination.cleanup()

    return results[0]


def main():
    parser = argparse.ArgumentParser()
    subparser = parser.add_subparsers(dest="command", required=True)
    subparser.add_parser("export")
    ingest_parser = subparser.add_parser("ingest")
    ingest_parser.add_argument("path")
    ingest_parser.add_argument("first_page", nargs="?", default=None)
    ingest_parser.add_argument("last_page", nargs="?", default=None)
    args = parser.parse_args()

    if args.command == "export":
        output = generate_pipeline()
    else:
        output = ingest(args.path, args.first_page, args.last_page)

    print(json.dumps(output.dict(), indent=4, sort_keys=True))


if __name__ == "__main__":
    main()
