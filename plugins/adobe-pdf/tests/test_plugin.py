# -*- coding: utf-8 -*-

#
# Copyright (c) 2021 Sorcero, Inc.
#
# This file is part of Sorcero's Language Intelligence platform
# (see https://www.sorcero.com).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#


import os
import pytest
import shutil
import tempfile

from ingestum import sources
from ingestum import transformers
from ingestum import engine
from ingestum import manifests
from ingestum import pipelines

from plugins import utils


__path__ = os.path.dirname(os.path.realpath(__file__))


pdf_source = sources.PDF(path="plugins/adobe-pdf/tests/data/test.pdf")

destinations = None


def setup_module():
    global destinations

    destinations = tempfile.TemporaryDirectory()
    os.mkdir("/tmp/ingestum")


def teardown_module():
    destinations.cleanup()
    shutil.rmtree("/tmp/ingestum")


def run_pipeline(pipeline, source):
    results, *_ = engine.run(
        manifest=manifests.Base(sources=[source]),
        pipelines=[pipeline],
        pipelines_dir=None,
        artifacts_dir=None,
        workspace_dir=None,
    )

    return results[0]


@pytest.mark.skipif(
    utils.skip_adobe_pdf, reason="INGESTUM_ADOBE_PDF_* variables not found"
)
def test_pdf_source_create_text_document_adobe():
    document = transformers.PDFSourceCreateTextDocumentAdobe(
        directory="/tmp/ingestum", first_page=1, last_page=3
    ).transform(source=pdf_source)
    assert document.dict() == utils.get_expected(
        "pdf_source_create_text_document_adobe", path=__path__
    )


@pytest.mark.skipif(
    utils.skip_adobe_pdf, reason="INGESTUM_ADOBE_PDF_* variables not found"
)
def test_pdf_source_create_text_document_adobe_no_pages():
    document = transformers.PDFSourceCreateTextDocumentAdobe(
        directory="/tmp/ingestum"
    ).transform(source=pdf_source)
    assert document.dict() == utils.get_expected(
        "pdf_source_create_text_document_adobe", path=__path__
    )


@pytest.mark.skipif(
    utils.skip_adobe_pdf, reason="INGESTUM_ADOBE_PDF_* variables not found"
)
def test_pdf_source_create_text_document_adobe_markers():
    document = transformers.PDFSourceCreateTextDocumentAdobe(
        directory="/tmp/ingestum",
        first_page=1,
        last_page=3,
        layout_markers={
            "title": "TITLE_MARKER",
            "header": "HEADER_MARKER",
            "paragraph": "PARAGRAPH_MARKER",
            "list_item": "LIST_ITEM_MARKER",
            "table": "TABLE_MARKER",
            "figure": "FIGURE_MARKER",
        },
    ).transform(source=pdf_source)
    assert document.dict() == utils.get_expected(
        "pdf_source_create_text_document_adobe_markers", path=__path__
    )


@pytest.mark.skipif(
    utils.skip_adobe_pdf, reason="INGESTUM_ADOBE_PDF_* variables not found"
)
def test_pdf_source_create_text_document_adobe_ocr():
    document = transformers.PDFSourceCreateTextDocumentAdobeOCR(
        directory="/tmp/ingestum",
        first_page=1,
        last_page=3,
        layout_markers={
            "title": "TITLE_MARKER",
            "header": "HEADER_MARKER",
            "paragraph": "PARAGRAPH_MARKER",
            "list_item": "LIST_ITEM_MARKER",
            "table": "TABLE_MARKER",
            "figure": "FIGURE_MARKER",
        },
    ).transform(source=pdf_source)
    assert document.dict() == utils.get_expected(
        "pdf_source_create_text_document_adobe_ocr", path=__path__
    )


@pytest.mark.skipif(
    utils.skip_adobe_pdf, reason="INGESTUM_ADOBE_PDF_* variables not found"
)
def test_pipeline_pdf_adobe():
    pipeline = pipelines.Base.parse_file(
        "plugins/adobe-pdf/tests/pipelines/pipeline_pdf_adobe.json"
    )
    source = manifests.sources.PDF(
        id="",
        pipeline=pipeline.name,
        first_page=1,
        last_page=3,
        location=manifests.sources.locations.Local(
            path="plugins/adobe-pdf/tests/data/test.pdf",
        ),
        destination=manifests.sources.destinations.Local(
            directory=destinations.name,
        ),
    )
    document = run_pipeline(pipeline, source)

    assert document.dict() == utils.get_expected("pipeline_pdf_adobe", path=__path__)


@pytest.mark.skipif(
    utils.skip_adobe_pdf or utils.skip_azure_pdf,
    reason="INGESTUM_ADOBE_PDF_* or INGESTUM_AZURE_CV_* variables not found",
)
def test_pdf_source_create_text_document_adobe_ocr_azure():
    document = transformers.PDFSourceCreateTextDocumentAdobeOCR(
        directory="/tmp/ingestum",
        first_page=1,
        last_page=3,
        layout_markers={
            "title": "TITLE_MARKER",
            "header": "HEADER_MARKER",
            "paragraph": "PARAGRAPH_MARKER",
            "list_item": "LIST_ITEM_MARKER",
            "table": "TABLE_MARKER",
            "figure": "FIGURE_MARKER",
        },
        engine="azure",
    ).transform(source=pdf_source)
    assert document.dict() == utils.get_expected(
        "pdf_source_create_text_document_adobe_ocr_azure", path=__path__
    )


@pytest.mark.skipif(
    utils.skip_adobe_pdf, reason="INGESTUM_ADOBE_PDF_* variables not found"
)
def test_pdf_source_create_text_document_hybrid_adobe():
    document = transformers.PDFSourceCreateTextDocumentHybrid(
        first_page=1, last_page=3, layout="multi", engine="adobe"
    ).transform(source=pdf_source)
    assert document.dict() == utils.get_expected(
        "pdf_source_create_text_document_hybrid_adobe", path=__path__
    )
